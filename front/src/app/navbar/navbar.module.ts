import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LeftComponent } from './left/left.component';
import { TopComponent } from './top/top.component';
import {routing} from "../app.routes";
import { NavbarUserComponent } from './navbar-user/navbar-user.component';

@NgModule({
  imports: [
    CommonModule,
    routing
  ],
  declarations: [LeftComponent, TopComponent, NavbarUserComponent],
  exports:[LeftComponent]
})
export class NavbarModule { }
