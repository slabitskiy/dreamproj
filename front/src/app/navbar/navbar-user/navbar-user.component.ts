import {Component, OnDestroy } from '@angular/core';
import {AppStorageServices} from "../../services/storage.services";
import {IUser} from "../../interfaces/iuser";
import {APP_CONFIGS} from "../../app.configs";

@Component({
  moduleId: module.id,
  selector: 'app-navbar-user',
  templateUrl: 'navbar-user.component.html',
  styleUrls: ['navbar-user.component.css']
})
export class NavbarUserComponent{
  user: IUser;
  userAvatar;
  constructor(private _storage: AppStorageServices) {
    this.initUser();
    this._storage.userUpdateEmmiter.subscribe(
      () =>{
        this.initUser();
      }
    );
  }
  private initUser(){
    this.user = this._storage.getUser;
    this.userAvatar = this._storage.getUser.avatar ? APP_CONFIGS.user.link + this._storage.getUser.avatar.name : APP_CONFIGS.user.avatar;
  }
}
