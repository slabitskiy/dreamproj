import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable }  from "rxjs/Observable";
import { LocalStorageService } from 'angular-2-local-storage';
import 'rxjs/Rx';
import 'rxjs/add/operator/map';

import { ROUTES } from './confgis';
import {APP_CONFIGS} from "../app.configs";
import {AppStorageServices} from "./storage.services";
import {Router} from "@angular/router";


@Injectable()
export class AuthService {

  constructor(private _http: Http,
              private _localStorageService: LocalStorageService,
              private _storage: AppStorageServices,
              private _router: Router

  ) {}

  register (args: Object): Observable<number>{
    return this._http.post(ROUTES.signup, args).map(res => res.status);
  }

  login (args: Object){
    return this._http.post(ROUTES.login, args)
                     .map((res) => {
                       if( res.status === 200) {
                          this._localStorageService.add(APP_CONFIGS.localStorage.name, true);
                       }
                       return res.json();
                     });
  }

  logout(){
    this._http
        .get(ROUTES.logout)
        .map( (res)=> {
          if(res.status === 200){
            this._localStorageService.add(APP_CONFIGS.localStorage.name, false);
            this._localStorageService.remove(APP_CONFIGS.localStorage.token);
            this._localStorageService.remove(APP_CONFIGS.localStorage.user);
            this._storage.setToken = '';
            this._storage.setUser = {user: ''};
            this._storage.loggedUpdate();
          }
          return res.status;
        })
        .subscribe(
          () => {},
          err => console.log(err),
          () => {
            this._router.navigate(['/login']);
          }
        );
  }

  session <T>(): Observable<T>{
    return this._http.get(ROUTES.session).map(res => res.json());
  }
}
