import passportLib from 'passport';
import Users from '../Models/User';
import mongoose from 'mongoose';
import async from 'async';
import _ from 'lodash';
import secrets from '../config/secrets';
import bcrypt from 'bcrypt-nodejs';
import jwt from 'jsonwebtoken';
import nJwt from 'njwt';
import moment from 'moment'
import Blacklist from  '../Models/TokenBlacklist.js'
const fileUpload = require('../Modules/fileUpload');
const helpers = require('../config/helpers');
module.exports = {
    home: (req, res) => {
        res.render('index.ejs', {
            isAuthenticated: req.isAuthenticated(),
            user: req.user ? req.user : null
        });
    },
    sess: (req, res) => {
        res.json({user: req.user, token: req.token});
        // res.json({user: req.user, token: req.token, status: true})
    },
    UserSignup: (req, res) =>{},
    UserSignupPost: (req, res, next) => {
        let body = req.body;
        helpers.SignupValidation(body, (errors)=>{
            if (errors) return helpers.newError( errors.details[0].message, 401, (error) => { return next(error); });
            Users
                .findOne({email: body.email})
                .exec((err, user) => {
                    if (err) return helpers.newError(err.msg, 500, (error) => { return next(error); });
                    if (user) return helpers.newError('User already exist', 409, (error)=>{ return next(error); });
                    let newUser = new Users(body);
                    newUser.pass = newUser.generateHash(body.password);
                    if (body.teacher == true) newUser.teacher = true;
                    newUser.save((err, user) => {
                        if (err) return helpers.newError(err.msg, 500, (error)=>{ return next(error); });
                        //helpers.createToken(user, (token) => {
                        //    req.headers['x-access-token'] = token;
                        //    res.status(200).send({message: 'OK', token:  token});
                        //})
                        return res.status(200).send('Ok');
                    })
                })
        });

    },
    login: (req, res) => {
        res.render('login.ejs')
    },
    Postlogin: (req, res, next) => {
        let body = req.body;
        helpers.LoginValidation(body, (errors)=> {
            if (errors) return helpers.newError(errors.details[0].message, 400, (error) => { return next(error); });
            Users
                .findOne({email: body.email})
                .populate('avatar')
                .exec((err, user)=>{
                    if (err) return helpers.newError(err.msg, 500, (error) => { return next(error) });
                    if (!user) return helpers.newError('User not found', 404, (error) => { return next(error) });
                    bcrypt.compare(body.password, user.pass, (err, result) => {
                        if (err) return helpers.newError(err.msg, 500, (error) => { return next(error) });
                        if (result === false) return helpers.newError("Bad credentials", 400, (error) => { return next(error) });
                        helpers.createToken(user, (token) => {
                            req.headers['x-access-token'] = token;
                            res.status(200).send({message: 'OK', token:  token, user: user});
                        })
                    });
                });
        });
    },
    logout: (req, res) => {
        req.user = null;
        req.headers['x-access-token'] = "";
        console.log("logout", req.headers['x-access-token']); 
        res.status(200).send("OK");
    },
    avatar: (req, res) =>{
        fileUpload.fileUpload(req, 'avatar', function(err, msg){
            if (err) console.log(err);
            res.json(msg);
        });
    },
    addTeacherProfile: (req, res) => {
        if (!req.body) return helpers.newError("Bad credentials", 400, (error) => { return next(error) });
        let body = req.body;
        Users
            .findById(req.user._id)
            .populate('avatar')
            .exec((err, user) => {
                if (err) return helpers.newError(err.msg, 500, (error) => { return next(error) });
                if (!user) return helpers.newError('User not found', 404, (error) => { return next(error) });
                user.set(body);

                if (user.city != "" &&
                    user.aboutSelf !="" &&
                    user.expirience!="" &&
                    user.venue!="" &&
                    user.levels != "" &&
                    user.skypeVenue!="" &&
                    user.hourlyRate !="" &&
                    user.educationInformation != "" &&
                    user.degree!=""
                   ){
                    user.validates = true;
                } else {
                    user.validates = false;
                }
                console.log("user.validates", user.validates);
                return user.save(err => {
                    if (err) helpers.newError(err.msg, 500, (error) => { return next(error) });
                    return res.status(200).send(user);
                });
            })
    },
    teachers: (req, res) => {
        Users
            .find({'teacher': true})
            .populate('avatar')
            .exec((err, users) => {
                if (err) return helpers.newError(err.msg, 500, (error) => { return next(error) });
                return res.status(200).send({users: users})
            })
    },
    TeacherProfile: (req, res, next) => {
        if (!req.params.id) return helpers.newError("Bad request", 400, (error) => { return next(error) });
        Users
            .findById(req.params.id)
            .populate('avatar')
            .exec(function(err, user){
                if (err)  return helpers.newError(err.msg, 500, (error) => { return next(error) });
                if (!user) return helpers.newError("User not found", 404, (error) => { return next(error) });
                return res.status(200).send({user:user});
            })
    },
    TeachersList: (req, res, next) => {
        let object = req.query;
        let query = {};
        let result = _.map(object, function(value, prop) {
            return { prop: prop, value: value };
        });
        result.forEach((item)=>{
            query[item.prop] = item.value;
        });
        query.teacher = true;
        query.validates = true;
        Users
            .find(query)
            .populate('avatar')
            .exec(function(err, teachers){
                if (err)  return helpers.newError(err.msg, 500, (error) => { return next(error) });
                if (!teachers) return helpers.newError("Teachers not found", 404, (error) => { return next(error) });
                return res.status(200).send({teachers:teachers});
            })
    }
};






