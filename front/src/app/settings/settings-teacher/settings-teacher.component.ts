import { Component} from '@angular/core';
import {FormGroup, FormBuilder, Validators} from "@angular/forms";
import {APP_CONFIGS} from "../../app.configs";
import {SettingsService} from "../../services/settings.service";
import {AppStorageServices} from "../../services/storage.services";
import { SelectComponent } from "ng2-select/ng2-select";
import {LocalStorageService} from "angular-2-local-storage";

@Component({
  moduleId: module.id,
  selector: 'app-settings-teacher',
  templateUrl: 'settings-teacher.component.html',
  styleUrls: ['settings-teacher.component.css'],
  providers: [SelectComponent]
})
export class SettingsTeacherComponent{
  userInfo = {
    name: this._storage.getUser.name,
    surname: this._storage.getUser.surname
  }
  profileForm: FormGroup;
  skillsForm: FormGroup;
  lessonsForm: FormGroup;
  educationForm: FormGroup;
  public levels:Array<string> = APP_CONFIGS.teacher.categories;
  public levelsSelected: Array<string> = this._storage.getUser.levels ? this._storage.getUser.levels : [];

  constructor(public fb: FormBuilder,
              private _settingsService: SettingsService,
              private _storage: AppStorageServices,
              private _localStorageService: LocalStorageService
  ) {
    console.log('teacher')
    this.profileForm = this.fb.group({
      city: [this._storage.getUser.city ? this._storage.getUser.city : '', [Validators.required]],
      aboutSelf: [this._storage.getUser.aboutSelf ? this._storage.getUser.aboutSelf : '', [Validators.required, Validators.minLength(100)]]
    });
    this.lessonsForm = this.fb.group({
      venue: [this._storage.getUser.venue ? this._storage.getUser.venue : '' , [Validators.required]],
      hourlyRate: [this._storage.getUser.hourlyRate ? this._storage.getUser.hourlyRate: '', [Validators.required]],
      skypeVenue: [this._storage.getUser.skypeVenue ? this._storage.getUser.skypeVenue: false]
    });
    this.educationForm = this.fb.group({
      educationInformation: [this._storage.getUser.educationInformation ? this._storage.getUser.educationInformation : '' , Validators.maxLength(400)],
      degree: [this._storage.getUser.degree ? this._storage.getUser.degree : '' , Validators.required]
    });
    this.skillsForm = this.fb.group({
      expirience: [this._storage.getUser.expirience ? this._storage.getUser.expirience : '', [Validators.required]],
      levels: [this.levelsSelected, [Validators.required]]
    });
  }


  selected(value:any):void {
    if(this.levelsSelected.length > 10){
      alert('Вы не можете выбрать больше специализаций');
      return;
    }
    this.levelsSelected.push(value.text);
  }
  removed(value:any):void {
    let indexValue = this.levelsSelected.indexOf(value.text);
    this.levelsSelected.splice(indexValue, 1);
  }


  saveGlobal(){
    this.userProfileUpdater(this.profileForm.value);
  }
  saveSkills(){
    this.userProfileUpdater(this.skillsForm.value)
  }
  saveLessons(){
    this.userProfileUpdater(this.lessonsForm.value);
  }
  saveEducation(){
    this.userProfileUpdater(this.educationForm.value);
  }


  private userProfileUpdater(data){
    this._settingsService.updateProfile(data).subscribe(
      data => {
        this._localStorageService.set(APP_CONFIGS.localStorage.user, data);
        this._storage.setUser = {user: data};
      },
      error => {
        console.log(error)
      },
      () =>{
        console.log('dddd')
        this._storage.loggedUpdate();
      }
    );
  }
  // categories = [{name: '1-4 classes', selected: true}, {name: '5-10 classes', selected: false}, {name: 'Ukrainian language',selected: true}];
  // this.initForm();
  // initForm() {
  //   let allCategories: FormArray = new FormArray([]);
  //   for (let i = 0; i < this.categories.length; i++) {
  //     let fg = new FormGroup({});
  //     fg.addControl(this.categories[i].name, new FormControl(this.categories[i].selected))
  //     allCategories.push(fg)
  //   }
  //   // console.log(allCategories);
  //   this.skillsForm = this.fb.group({
  //     expirience: ['', [Validators.required]],
  //     'categories': allCategories
  //   })
  // }
}
