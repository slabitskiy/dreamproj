import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {DashboardComponent} from "./dashboard.component";
import { DashboardContentComponent } from './dashboard-content/dashboard-content.component';
import { DashboardLoginComponent } from './dashboard-login/dashboard-login.component';
import { DashboardNotloginComponent } from './dashboard-notlogin/dashboard-notlogin.component';
import {NavbarModule} from "../navbar/navbar.module";
import {DashboardService} from "../services/dashboard.service";
import { DashboardUserComponent } from './dashboard-user/dashboard-user.component';
import {NotificationsModule} from "../notifications/notifications.module";
import {RouterModule} from "@angular/router";
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  imports: [
    NavbarModule,
    CommonModule,
    RouterModule,
    ReactiveFormsModule,
    FormsModule
  ],
  providers: [
    DashboardService
  ],
  entryComponents: [ DashboardLoginComponent, DashboardNotloginComponent],
  declarations: [DashboardComponent, DashboardContentComponent, DashboardLoginComponent, DashboardNotloginComponent, DashboardUserComponent]
})
export class DashboardModule { }
