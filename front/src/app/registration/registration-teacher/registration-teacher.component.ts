import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-registration-teacher',
  templateUrl: 'registration-teacher.component.html',
  styleUrls: ['registration-teacher.component.css']
})
export class RegistrationTeacherComponent implements OnInit {
  role: boolean = true;
  constructor() { }

  ngOnInit() {
  }

}
