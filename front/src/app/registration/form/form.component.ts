import {Component, OnInit, Input} from '@angular/core';
import {FormGroup, FormBuilder, Validators, AbstractControl} from "@angular/forms";
import {FormValidators} from "../formValidators";
import {AuthService} from "../../services/auth.service";

@Component({
  moduleId: module.id,
  selector: 'registration-form',
  templateUrl: 'form.component.html',
  styleUrls: ['form.component.css'],
  providers: [ AuthService]
})
export class FormComponent implements OnInit {
  @Input() role: boolean;
  form: FormGroup;
  requestStatus: {error: number,
                  success: number} = {error: 0, success: 0};
  formValidator: FormValidators = new FormValidators();

  constructor(public fb: FormBuilder,
              private _auth: AuthService,) {
    this.form = this.fb.group({
      name:  ['', Validators.required],
      surname:  ['', Validators.required],
      email:  ['', [this.formValidator.emailMatch, Validators.required]],
      passwords: this.fb.group({
        password: ['', [ Validators.required, Validators.minLength(4) ]],
        repassword: ['', [ Validators.required, Validators.minLength(4) ]]
      },{validator: this.passMatch})
    });
  }

  ngOnInit() {
  }

  private passMatch(c: AbstractControl){
    return ((c.get('password').value === c.get('repassword').value ) && c.get('repassword').value.length !=0) ? {matches: false} : {matches: true};
  }
  sendData(){
    let obj: Object = {
      email: this.form.value.email,
      name: this.form.value.name,
      surname: this.form.value.surname,
      password: this.form.value.passwords.password,
      confirPassword: this.form.value.passwords.repassword,
      teacher: (this.role)? true : false
    }
    console.log(obj, this.role, typeof this.role)
    this._auth.register(obj).subscribe(
      (data) => {
        this.requestStatus.success = data;
        // console.log(data)
        if(data === 200) {
          this.form.patchValue({
            email: '',
            surname: '',
            name: '',
            passwords: {
              password: '',
              repassword: ''
            }
          }); // костиль,  this.form.reset(); не паше,  проблема в zone.js
        }
      },
      (err)=> {
        this.requestStatus.error = err.status;
      }
    );
  }
}
