let mongoose = require('mongoose');
let bcrypt   = require('bcrypt-nodejs');

let userSchema = new mongoose.Schema({
    email: {
        unique: true,
        type: String
    },
    pass: String,
    name: String,
    surname: String,
    teacher:{ type: Boolean, default: false },
    avatar: {
        type: mongoose.Schema.Types.ObjectId,
        ref: 'File'
    },
    city: String,
    aboutSelf: String,
    expirience: String,
    levels: [],
    venue: String,
    skypeVenue: Boolean,
    hourlyRate: String,
    educationInformation: String,
    degree: String,
    validates: {
        type: Boolean,
        default: false
    }
});

userSchema.methods.generateHash = (pass) => {
    return bcrypt.hashSync(pass, bcrypt.genSaltSync(8), null);
};

module.exports = mongoose.model('User', userSchema);
