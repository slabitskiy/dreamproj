import { Injectable } from '@angular/core';
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import {AppStorageServices} from "../services/storage.services";


@Injectable()
export class SettingsResolveService implements Resolve<boolean>{
  constructor(private _storage: AppStorageServices) { }
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean{
    return this._storage.getUser.teacher;
  }
}

