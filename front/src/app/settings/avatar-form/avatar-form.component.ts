import { Component } from '@angular/core';
import {LocalStorageService} from "angular-2-local-storage";
import {AppStorageServices} from "../../services/storage.services";
import {APP_CONFIGS} from "../../app.configs";
import {SettingsService} from "../../services/settings.service";

@Component({
  moduleId: module.id,
  selector: 'app-avatar-form',
  templateUrl: 'avatar-form.component.html',
  styleUrls: ['avatar-form.component.css']
})
export class AvatarFormComponent {
  renderedImage = {
    image: this._storage.getUser.avatar ? APP_CONFIGS.user.link + this._storage.getUser.avatar.name : APP_CONFIGS.user.avatar,
    name:  this._storage.getUser.avatar ? this._storage.getUser.avatar.original_name :'Choose your image'
  };
  private formFiles;
  constructor(private _storage: AppStorageServices,
              private _localStorageService: LocalStorageService,
              private _settingsService: SettingsService) { }

  select(file){
    let reader = new FileReader(),
      self = this,
      fileTarget = file.target.files[0];
    this.formFiles = fileTarget;
    reader.onload = (e) => {
      self.renderedImage.image = (e.target as any).result;
      self.renderedImage.name = fileTarget.name;
    };
    reader.readAsDataURL(fileTarget);
  }

  submitFile(){
    let fd: FormData = new FormData();
    fd.append('file', this.formFiles, this.formFiles.name)

    this._settingsService.avatarSet(fd).subscribe(
      data => {
        let objAssign = Object.assign(this._localStorageService.get(APP_CONFIGS.localStorage.user), {avatar: data.data});
        this._localStorageService.set(APP_CONFIGS.localStorage.user, objAssign);
        this._storage.userUpdate(objAssign);
      },
      error => console.log(error)
    );
  }

}
