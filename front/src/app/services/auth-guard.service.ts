import { Injectable } from '@angular/core';
import {CanActivate, Router} from "@angular/router";
import { LocalStorageService } from 'angular-2-local-storage';
import {AppStorageServices} from "./storage.services";
import {APP_CONFIGS} from "../app.configs";


@Injectable()
export class AuthGuardService  implements CanActivate {

  constructor(private _localStorageService: LocalStorageService,
              private _router: Router,
              private _storage: AppStorageServices) { }

  canActivate(){
    let isLogged = this._localStorageService.get(APP_CONFIGS.localStorage.name);
    if(isLogged && this._storage.getToken) {
      this._storage.setUser = {user: this._localStorageService.get(APP_CONFIGS.localStorage.user)};
      return true;
    }else{
      this._router.navigate(['/login']);
      return false;
    }
  }

}
