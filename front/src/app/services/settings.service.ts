import { Injectable } from '@angular/core';
import {Http, Response, Headers, RequestOptions} from "@angular/http";
import {Observable} from "rxjs";
import {ROUTES} from "./confgis";

@Injectable()
export class SettingsService {

  constructor(private _http: Http) { }
  avatarSet (formData: FormData): Observable<any>{
    return this._http.post(ROUTES.settings.avatar, formData).map((res) => res.json());
  }
  updateProfile(data): Observable<any>{
    return this._http.put(ROUTES.settings.updateInformation, data).map(res => res.json());
  }
}
