import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SettingsTeacherComponent } from './settings-teacher/settings-teacher.component';
import { SettingsResolveService } from "./settings-resolve";
import { SettingsComponent } from './settings.component';
import { SettingsUserComponent } from './settings-user/settings-user.component';
import {ReactiveFormsModule, FormsModule} from "@angular/forms";
import {NavbarModule} from "../navbar/navbar.module";
import {SettingsService} from "../services/settings.service";
import { AvatarFormComponent } from './avatar-form/avatar-form.component';

import { SelectModule } from "ng2-select/ng2-select";
import {NotificationsModule} from "../notifications/notifications.module";

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    NavbarModule,
    SelectModule,
    // NotificationsModule

    // Sett
  ],
  entryComponents: [SettingsTeacherComponent, SettingsUserComponent ],
  declarations: [SettingsTeacherComponent, SettingsComponent, SettingsUserComponent, AvatarFormComponent],
  providers: [
    SettingsResolveService,
    SettingsService
  ]
})
export class SettingsModule { }
