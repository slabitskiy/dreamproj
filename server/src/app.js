let express = require('express');
let app = express();
let server = require('http').createServer(app);
let io = require('socket.io').listen(server);
let secrets = require('./config/secrets');
let bodyParser = require('body-parser');
let helpers = require('./config/helpers');
let busboy = require('busboy-body-parser');
let errorHandler = require('errorhandler');
let expressValidator = require('express-validator');
let cookieParser = require('cookie-parser');
let mongoose = require('mongoose');
let logger = require('morgan');
let User = require('./Models/User');
let path = require('path');
let vLogger = require('log4js').getLogger();

app.use(logger('dev'));
app.use(errorHandler());
// app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());
app.use(busboy());
app.use(cookieParser());
app.use(expressValidator());
app.set('view engine', 'ejs');
app.set('views', __dirname + '/public/views');
app.set('io', io);
app.set('port', secrets.port);
app.use(express.static(path.join(__dirname)));
mongoose.connect(secrets.db);
mongoose.connection
    .on('error', function() {
        vLogger.warn('MongoDB Connection Error. Make sure MongoDB is running.');
    })
    .on('connected', function(){
        vLogger.info('MongoDB connected successfully.');
        require('./config/routes')(app, helpers)
    });
require('./config/routes.js')(app);
server.listen(app.get('port'));
console.log('Server running');

