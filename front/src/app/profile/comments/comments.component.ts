import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-profile-comments',
  templateUrl: 'comments.component.html',
  styleUrls: ['comments.component.css']
})
export class CommentsComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
