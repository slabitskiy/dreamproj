import {Component, OnInit, ViewContainerRef, ComponentFactoryResolver, ViewChild} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {DashboardLoginComponent} from "./dashboard-login/dashboard-login.component";
import {DashboardNotloginComponent} from "./dashboard-notlogin/dashboard-notlogin.component";

@Component({
  moduleId: module.id,
  selector: 'app-dashboard',
  templateUrl: 'dashboard.component.html',
  styleUrls: ['dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  @ViewChild('dashboard', {read: ViewContainerRef}) dashboard;

  constructor( private componentFactoryResolver: ComponentFactoryResolver,
               private activatedRoute: ActivatedRoute,
               private viewContainerRef: ViewContainerRef) {

    const dashboardComponentResolved = this.activatedRoute.snapshot.data['dashboard'] ? DashboardLoginComponent : DashboardNotloginComponent;
    const factory = this.componentFactoryResolver.resolveComponentFactory(dashboardComponentResolved);
    const ref = this.viewContainerRef.createComponent(factory);

    ref.changeDetectorRef.detectChanges();
  }

  ngOnInit() {
  }

}
