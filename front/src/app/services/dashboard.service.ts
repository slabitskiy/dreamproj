import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import {ROUTES} from "./confgis";

@Injectable()
export class DashboardService {

  constructor(private _http: Http) { }

  users(): Observable<any>{
    return this._http.get(ROUTES.users).map(res => res.json());
  }
}
