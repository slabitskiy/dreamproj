import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NotificationsVerifyComponent } from './notifications-verify/notifications-verify.component';

@NgModule({
  imports: [
    CommonModule
  ],
  declarations: [NotificationsVerifyComponent],
  exports: [NotificationsVerifyComponent]
})
export class NotificationsModule { }
