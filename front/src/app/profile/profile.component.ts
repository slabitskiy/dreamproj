import {Component, OnInit, OnDestroy} from '@angular/core';
import {ProfileService} from "../services/profile.service";
import {ActivatedRoute} from "@angular/router";
import {IUser} from "../interfaces/iuser";
import {APP_CONFIGS} from "../app.configs";

@Component({
  moduleId: module.id,
  selector: 'app-profile',
  templateUrl: 'profile.component.html',
  styleUrls: ['profile.component.css']
})
export class ProfileComponent implements OnInit, OnDestroy {
  private sub: any;
  user: IUser;
  constructor(private route: ActivatedRoute,
              private _profileService: ProfileService) {}

  ngOnInit() {
    this.sub = this.route.params.subscribe(params => {
      this._profileService.getUser(params["id"])
        .do((res: any) => {
          if(res.user.avatar){
            let avatarDef = res.user.avatar.name;
            res.user.avatar.name = `${APP_CONFIGS.user.link}${avatarDef}`;
          }else{
            res.user['avatar'] = {};
            res.user.avatar['name'] =  APP_CONFIGS.user.avatar;
          }
          return res;
        })
        .subscribe(
          res => {
            this.user = res.user;
          },
          err => console.log(err)
        );
    });
  }
  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
