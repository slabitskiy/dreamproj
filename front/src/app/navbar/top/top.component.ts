import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-navbar-top',
  templateUrl: 'top.component.html',
  styleUrls: ['top.component.css']
})
export class TopComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
