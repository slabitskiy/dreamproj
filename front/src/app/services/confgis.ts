const BASE__ROUTE = 'http://localhost:5000/';

interface IRoutes {
  login: string,
  logout: string,
  signup: string,
  session: string
  settings: {
    avatar: string,
    updateInformation: string
  },
  users: string,
  teacher: string
}

export const ROUTES: IRoutes = {
  login: BASE__ROUTE + 'login',
  logout: BASE__ROUTE + 'logout',
  signup: BASE__ROUTE + 'user/signup',
  session: BASE__ROUTE + 'sess',
  settings: {
    avatar: BASE__ROUTE + 'avatar',
    updateInformation: BASE__ROUTE + 'profile'
  },
  users: BASE__ROUTE + 'teachers',
  teacher: BASE__ROUTE +'teacher'
}



