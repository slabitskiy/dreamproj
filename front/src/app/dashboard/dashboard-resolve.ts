import { Injectable } from '@angular/core';
import {Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from "@angular/router";
import {LocalStorageService} from "angular-2-local-storage";
import {APP_CONFIGS} from "../app.configs";


@Injectable()
export class DashboardResolveService implements Resolve<boolean>{
  role = this._localStorageService.get(APP_CONFIGS.localStorage.name);
  constructor(private _localStorageService: LocalStorageService) { }
  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): boolean{
    return <boolean>this.role;
  }
}

