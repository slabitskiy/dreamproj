/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { SettingsResolveService } from './settings-resolve';

describe('SettingsResolveService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SettingsResolveService]
    });
  });

  it('should ...', inject([SettingsResolveService], (service: SettingsResolveService) => {
    expect(service).toBeTruthy();
  }));
});
