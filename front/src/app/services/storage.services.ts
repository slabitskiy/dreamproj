import {Injectable, EventEmitter} from "@angular/core";
import {IUser} from "../interfaces/iuser";
import {Observable} from "rxjs";

@Injectable()
export class AppStorageServices {
  private token: string;
  private user: IUser;

 public userUpdateEmmiter: EventEmitter<any> = new EventEmitter();
 public loggedListener: EventEmitter<any> = new EventEmitter();
  constructor(){}

  set setToken(token){
    this.token = token;
  }
  set setUser(user: any){
    this.user = user.user;
  }
  userUpdate(user){
    this.user = user;
    this.userUpdateEmmiter.emit(user);
  }
  get getUser(){
    return this.user;
  }
  get getToken(){
    return this.token;
  }

  loggedUpdate(){
    // console.log('emmit')
    this.loggedListener.emit(true);
  }
}
