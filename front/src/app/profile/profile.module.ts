import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import {ProfileComponent} from "./profile.component";
import {NavbarModule} from "../navbar/navbar.module";
import {ProfileService} from "../services/profile.service";
import { MainComponent } from './main/main.component';
import { CommentsComponent } from './comments/comments.component';

@NgModule({
  imports: [
    CommonModule,
    NavbarModule
  ],
  providers: [
    ProfileService
  ],
  exports: [ProfileComponent],
  declarations: [ProfileComponent, MainComponent, CommentsComponent]
})
export class ProfileModule { }
