import {Component, OnInit, Input, AfterViewChecked, AfterContentInit, AfterViewInit} from '@angular/core';
import {IUser} from "../../interfaces/iuser";

@Component({
  moduleId: module.id,
  selector: 'app-profile-main',
  templateUrl: 'main.component.html',
  styleUrls: ['main.component.css']
})
export class MainComponent implements OnInit, AfterViewChecked, AfterContentInit, AfterViewInit {
  @Input() profile: IUser;
  constructor() { }

  ngOnInit() {
    // console.log(this.profile)
  }
  ngAfterViewChecked() {
    // console.log(this.profile)
  }
  ngAfterContentInit() {
    console.log('aftercontentinit',this.profile)
  }
  ngAfterViewInit(){
    console.log('afterviewinit',this.profile)
  }

}
