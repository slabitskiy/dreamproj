import { ModuleWithProviders } from '@angular/core';
import { Routes, RouterModule, Route } from '@angular/router';
import { RegistrationComponent } from './registration/registration.component';
import { RegistrationUserComponent } from './registration/registration-user/registration-user.component';
import { RegistrationTeacherComponent } from './registration/registration-teacher/registration-teacher.component';
import { LoginComponent } from './login/login.component';
import {SettingsResolveService} from "./settings/settings-resolve";
import {SettingsComponent} from "./settings/settings.component";
import {AuthGuardService} from "./services/auth-guard.service";
import {NonAuthGuardService} from "./services/non-auth-guard.service";
import {DashboardComponent} from "./dashboard/dashboard.component";
import {DashboardResolveService} from "./dashboard/dashboard-resolve";
import {ProfileComponent} from "./profile/profile.component";


const FAIL_PATH: Route = {
  path: '**', component: LoginComponent
};

export const routes: Routes = [
  {path: '', pathMatch: 'full', redirectTo: 'login' },
  {path: 'login', component: LoginComponent , canActivate: [NonAuthGuardService]},
  {path: 'registration', component: RegistrationComponent, canActivate: [NonAuthGuardService]},
  {path: 'registration/user', component: RegistrationUserComponent, canActivate: [NonAuthGuardService]},
  {path: 'registration/teacher', component: RegistrationTeacherComponent},
  {path: 'settings', component: SettingsComponent, resolve: { settings: SettingsResolveService}, canActivate: [AuthGuardService]},
  {path: 'dashboard', component: DashboardComponent, resolve: {dashboard: DashboardResolveService}},
  {path: 'user/:id', component: ProfileComponent},
  FAIL_PATH
];

export const routesChild: Routes = [
  // {path: '', redirectTo: 'login', canActivate: [AuthGuardService],
  //   children: [
  //     {path: 'settings', component: SettingsComponent, resolve: { settings: SettingsResolveService }},
  //     FAIL_PATH
  //   ]
  // },
  // FAIL_PATH
];
export const routing: ModuleWithProviders  = RouterModule.forRoot(routes);
export const routingChild: ModuleWithProviders = RouterModule.forChild(routesChild);
