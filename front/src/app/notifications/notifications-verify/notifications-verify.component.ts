import { Component } from '@angular/core';
import {AppStorageServices} from "../../services/storage.services";

@Component({
  moduleId: module.id,
  selector: 'app-notifications-verify',
  templateUrl: 'notifications-verify.component.html',
  styleUrls: ['notifications-verify.component.css']
})
export class NotificationsVerifyComponent {
  isHide: boolean = false;
  verified: boolean = !this._storage.getUser.teacher? true : this._storage.getUser.validates ;
  constructor(private _storage: AppStorageServices) {
    // console.log(!this._storage.getUser.teacher, this._storage.getUser.validates, this.verified);
    this._storage.loggedListener.subscribe(
      () =>{
        this.verified = this._storage.getUser.teacher? true : this._storage.getUser.validates;
      }
    );
  }


  hideNotification(){
    this.isHide = true;
  }
}
