let _ = require('lodash');
let passport = require('passport');
let LocalStrategy = require('passport-local').Strategy;
let User = require('../Models/User');
let secrets = require('./secrets');
let async = require('async');
let jwt = require('jsonwebtoken');
import moment from 'moment'
import Joi from 'joi';
module.exports = {
    tokenInspection:  (req, res, next) => {
        req.user = null;
        let token = req.headers['x-access-token'];
        console.log(token);
        if (token) {
            jwt.verify(token, secrets.sessionSecret, (err, decoded) => {
                if (err) return res.status(501).send("Bad token");
                // if (err) return res.status(200).send({msg: "Bad token", status: false});
                User
                    .findById(decoded.data)
                    .exec((err, user)=> {
                        if (err) return res.status(501).send("Bad token");
                        // if (err) return res.status(200).send({msg: "Bad token", status: false});
                        if (user) {
                            req.user = user;
                            return next()
                        }
                        return res.status(404).send("User not found");
                        // return res.status(200).send({msg: "User not found", status: false});
                    })
            });
        } else {
            // return res.status(200).send({msg: "Token not found", status: false});
            return res.status(404).send("Token not found");
        }

    },
    newError: (msg, status, done) => {
        let error = {
            message: msg,
            status: status
        };
        done(error);
    },
    createToken: (user, done) => {
        let token = jwt.sign({
            exp: moment().add(3, 'days').unix(),
            iat: moment().unix(),
            data: user._id
        }, secrets.sessionSecret);
        if (done) return done(token);
    },
    SignupValidation: (user, done) => {
        console.log("Calling helper");
        var schema = Joi.object().keys({
            name: Joi.string().alphanum().min(3).max(30).required(),
            surname: Joi.string().alphanum().min(3).max(30).required(),
            email: Joi.string().email().required(),
            password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/),
            confirPassword: Joi.any().valid(Joi.ref('password')).required().options({ language: { any: { allowOnly: 'must match password' } } }),
            teacher: Joi.boolean().allow('')
        });
        Joi.validate(user, schema, (err) => {
            if (err) return done(err);
            done(null);
        });
    },
    LoginValidation: (user, done) => {
        var schema = Joi.object().keys({
            email: Joi.string().email().required(),
            password: Joi.string().regex(/^[a-zA-Z0-9]{3,30}$/)
        });
        Joi.validate(user, schema, (err) => {
            if (err) return done(err);
            done(null);
        });
    }
};


