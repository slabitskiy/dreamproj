var mongoose = require('mongoose');
var blacklistSchema = new mongoose.Schema({
    token: String
});

module.exports = mongoose.model('TokenBlacklist', blacklistSchema);
