const upload = require('../Modules/upload');
const File = require('../Models/File');
const randomstring = require("randomstring");
const fs = require('fs');

exports.fileUpload = (req, type, done) => {
    var file_name =  randomstring.generate(6) + new Date().getTime() + req.files.file.name;
    var shortPath = "/../uploads/" + type + '/' + file_name;
    var newPath = __dirname + "/../uploads/" + type + '/' + file_name;
    var model = File;
    if (req.files.file.data){
        fs.writeFile(newPath, req.files.file.data, (err) => {
            if (err) { done(err) }
            upload.uploadFile(model, req.user._id, req.files.file, file_name, type, shortPath , (err, file) => {
                if (err) return res.status(500).send(err);
                if (type == 'avatar') {
                    req.user.avatar = file._id;
                    req.user.save();
                }
                done(null, {message: 'File was added!', data: file})
            });
        });
    }
};
