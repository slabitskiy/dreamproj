import { Component, OnInit } from '@angular/core';
import {DashboardService} from "../../services/dashboard.service";
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import {IUser} from "../../interfaces/iuser";
import {APP_CONFIGS} from "../../app.configs";


@Component({
  moduleId: module.id,
  selector: 'app-dashboard-content',
  templateUrl: 'dashboard-content.component.html',
  styleUrls: ['dashboard-content.component.css']
})
export class DashboardContentComponent implements OnInit {
  users;
  notificationHide: boolean;
  searchValue: string;
  constructor(private _dashboardService: DashboardService,
              public fb: FormBuilder) {}

  ngOnInit() {
    this._dashboardService.users()
      .map((res, idx) =>{
        res.teachers.forEach((el) => {
          if(el.avatar){
            let avatarDef = el.avatar.name;
            el.avatar.name = `${APP_CONFIGS.user.link}${avatarDef}`;
          }else{
            el['avatar'] = {};
            el.avatar['name'] =  APP_CONFIGS.user.avatar;
          }
        });
        // res.teachers[idx]['avatar']['name'] = `${APP_CONFIGS.user.link}${res.teachers[idx]['avatar']['name']}`;
        return res;
      })
      .subscribe(
        data => {
          this.users = data.teachers.length == 0 ? [] :  data.teachers;
          this.notificationHide = data.teachers.length == 0 ? true : false;
        }
    );



    console.log(this.searchValue);
  }

}
