import { Component, OnInit } from '@angular/core';

@Component({
  moduleId: module.id,
  selector: 'app-dashboard-login',
  templateUrl: 'dashboard-login.component.html',
  styleUrls: ['dashboard-login.component.css']
})
export class DashboardLoginComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

}
