/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { NonAuthGuardService } from './non-auth-guard.service';

describe('NonAuthGuardService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [NonAuthGuardService]
    });
  });

  it('should ...', inject([NonAuthGuardService], (service: NonAuthGuardService) => {
    expect(service).toBeTruthy();
  }));
});
