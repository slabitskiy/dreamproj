import { Component } from '@angular/core';
import {AuthService} from "../../services/auth.service";

@Component({
  moduleId: module.id,
  selector: 'app-navbar-left',
  templateUrl: 'left.component.html',
  styleUrls: ['left.component.css']
})
export class LeftComponent {

  constructor(private _auth: AuthService) { }
  logout(){
    this._auth.logout();
  }
}
