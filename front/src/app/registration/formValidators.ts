import { FormControl } from '@angular/forms';


export class FormValidators {
  constructor() {
  }
  emailMatch(c: FormControl) {
    let re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(c.value) ? {validation: false} : {validation: true};
  }

}
