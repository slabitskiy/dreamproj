import {Component, OnInit, Input} from '@angular/core';
import {IUser} from "../../interfaces/iuser";

@Component({
  moduleId: module.id,
  selector: 'app-dashboard-user',
  templateUrl: 'dashboard-user.component.html',
  styleUrls: ['dashboard-user.component.css']
})
export class DashboardUserComponent implements OnInit {
  @Input() user: IUser;
  constructor() { }

  ngOnInit() {
  }

}
