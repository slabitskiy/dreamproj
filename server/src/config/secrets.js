let env = process.env.NODE_ENV || 'dev';

var config = {
    production: {
    },
    dev: {
        port: process.env.PORT || 5000,
        db: 'mongodb://localhost:27017/Dream',
        sessionSecret: 'superSecret',
        tokenName: 'superToken'
    }
};


module.exports = config[env];