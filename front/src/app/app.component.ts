import { Component, OnInit } from '@angular/core';
import {AppStorageServices} from "./services/storage.services";
import { LocalStorageService } from 'angular-2-local-storage';
import {APP_CONFIGS} from "./app.configs";
import {AuthService} from "./services/auth.service";
import {IUser} from "./interfaces/iuser";

@Component({
  moduleId: module.id,
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.css']
})
export class AppComponent implements OnInit{
  private token = this._localStorageService.get(APP_CONFIGS.localStorage.token);
  private user: IUser|any = this._localStorageService.get(APP_CONFIGS.localStorage.user);
  isLogged = this._localStorageService.get(APP_CONFIGS.localStorage.name);
  constructor(private _storage: AppStorageServices,
              private _localStorageService: LocalStorageService,
              private _auth: AuthService){
    if(this.token || this.user){
      this._storage.setToken = this.token;
      this._storage.setUser = {user: this.user};
    }
    this._storage.loggedListener.subscribe(
      ()=>{
        this.isLogged = this._localStorageService.get(APP_CONFIGS.localStorage.name);
      }
    )
  }

  ngOnInit(){
    if(this.token){
      this._auth.session().subscribe(
        () => {},
        () =>{
          this._auth.logout();
        }
      );
    }
  }
}
