export interface IUser {
  avatar:{
    original_name: null | string ,
    name:  null | string
  },
  name: string,
  surname: string,
  email: string,
  password: string,
  teacher: boolean,
  city: string,
  aboutSelf: string,
  expirience: string,
  levels: string[],
  venue: string,
  skypeVenue: boolean,
  hourlyRate: string,
  educationInformation: string,
  degree: string,
  validates: boolean
}
