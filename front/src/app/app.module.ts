import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import {HttpModule, RequestOptions} from '@angular/http';

import { routing,routingChild } from './app.routes';

import { AppComponent } from './app.component';
import {BaseHeaderOptions} from "./services/services.header";
import {RegistrationModule} from "./registration/registration.module";
import {LoginModule} from "./login/login.module";
import {SettingsModule} from "./settings/settings.module";
import {LocalStorageModule, LocalStorageService} from 'angular-2-local-storage';
import {APP_CONFIGS} from "./app.configs";
import {AuthGuardService} from "./services/auth-guard.service";
import {AuthService} from "./services/auth.service";
import {AppStorageServices} from "./services/storage.services";
import {NonAuthGuardService} from "./services/non-auth-guard.service";
import {NavbarModule} from "./navbar/navbar.module";
import {DashboardModule} from "./dashboard/dashboard.module";
import {DashboardResolveService} from "./dashboard/dashboard-resolve";
import {NotificationsModule} from "./notifications/notifications.module";
import {ProfileModule} from "./profile/profile.module";

@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    HttpModule,
    routing,
    routingChild,
    RegistrationModule,
    LoginModule,
    SettingsModule,
    NavbarModule,
    DashboardModule,
    NotificationsModule,
    ProfileModule,
    LocalStorageModule.withConfig({
      prefix: APP_CONFIGS.localStorage.app,
      storageType: 'localStorage'
    })
  ],
  providers: [
    AuthGuardService,
    AuthService,
    LocalStorageService,
    AppStorageServices,
    NonAuthGuardService,
    DashboardResolveService,
    {provide: RequestOptions, useClass: BaseHeaderOptions }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
