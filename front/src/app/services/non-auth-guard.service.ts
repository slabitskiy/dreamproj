import { Injectable } from '@angular/core';
import {CanActivate, Router} from "@angular/router";
import {LocalStorageService} from "angular-2-local-storage";
import {APP_CONFIGS} from "../app.configs";

@Injectable()
export class NonAuthGuardService implements CanActivate{

  constructor(private _localStorageService: LocalStorageService,
              private _router: Router) { }

  canActivate(){
    let isLogged = this._localStorageService.get(APP_CONFIGS.localStorage.name);
    if(isLogged){
      this._router.navigate(['/settings']);
      return false;
    }
    return true;
  }
}
