module.exports = (app, passport) => {
    let passportModule = require('passport');
    let randomstring = require("randomstring");
    let fs = require('fs');
    let path = require('path');
    let async = require('async');
    let main_controller = require('../controllers/main.js');
    let helpers = require('../config/helpers');

    app.all('*', function(req, res, next) {
        res.header("Access-Control-Allow-Origin", "*");
        res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
        res.header('Access-Control-Allow-Headers', 'Content-type,Accept,X-Access-Token,X-Key');
        if (req.method == 'OPTIONS') {
            res.status(200).end();
        } else {
            next();
        }
    });
    app.get('/',  main_controller.home);
    app.get('/sess', helpers.tokenInspection,  main_controller.sess);
    app.get('/teacher/id:id',  main_controller.TeacherProfile);
    app.get('/teachers',  main_controller.TeachersList);
    app.get('/user/signup', main_controller.UserSignup);
    app.post('/user/signup',  main_controller.UserSignupPost);
    app.get('/login',  main_controller.login);
    app.put('/profile', helpers.tokenInspection, main_controller.addTeacherProfile);
    app.post('/login', main_controller.Postlogin);
    app.get('/logout', main_controller.logout);
    app.get('/teachers', main_controller.teachers);
    app.post('/avatar', helpers.tokenInspection, main_controller.avatar);

    app.use((err, req, res, next) => {
       if (err){
           return res.status(err.status).send(err.message);
       }
       next();
    })
};
