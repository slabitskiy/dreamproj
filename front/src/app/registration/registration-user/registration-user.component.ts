import { Component, OnInit } from '@angular/core';
// import { AbstractControl, FormGroup, FormBuilder, Validators } from '@angular/forms';
// import { RegistrationService } from '../../services/registration.service';
// import { FormValidators } from '../formValidators';
import {Router} from "@angular/router";


@Component({
  moduleId: module.id,
  selector: 'app-registration-user',
  templateUrl: 'registration-user.component.html',
  styleUrls: ['registration-user.component.css']
})
// providers: [ RegistrationService]
export class RegistrationUserComponent implements OnInit {
  role: boolean = false;
  // form: FormGroup;
  // errorStatus: number;
  // formValidator: FormValidators = new FormValidators();

  // public fb: FormBuilder,
  // private _auth: RegistrationService,
  constructor(private _router: Router) {
    // this.form = this.fb.group({
    //   name:  ['', Validators.required],
    //   surname:  ['', Validators.required],
    //   email:  ['', [this.formValidator.emailMatch, Validators.required]],
    //   passwords: this.fb.group({
    //     password: ['', [ Validators.required, Validators.minLength(4) ]],
    //     repassword: ['', [ Validators.required, Validators.minLength(4) ]]
    //   },{validator: this.passMatch})
    // });
  }

  ngOnInit() {}

  // private passMatch(c: AbstractControl){
  //   return ((c.get('password').value === c.get('repassword').value ) && c.get('repassword').value.length !=0) ? {matches: false} : {matches: true};
  // }
  // sendData(){
  //   let obj: Object = {
  //     email: this.form.value.email,
  //     name: this.form.value.name,
  //     surname: this.form.value.surname,
  //     password: this.form.value.passwords.password,
  //     confirPassword: this.form.value.passwords.repassword,
  //     teacher: false
  //   }
  //   this._auth.register(obj).subscribe(
  //     (data) => {
  //       this.errorStatus = data;
  //       if(data === 200) {
  //         this.form.patchValue({
  //           email: '',
  //           surname: '',
  //           name: '',
  //           passwords: {
  //             password: '',
  //             repassword: ''
  //           }
  //         }); // костиль,  this.form.reset(); не паше,  проблема в zone.js
  //       }
  //     },
  //     (err)=> {
  //       this.errorStatus = err.status;
  //     }
  //   );
  // }

}
