import { NgModule } from '@angular/core';
import { HttpModule } from '@angular/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import {routing, routingChild} from '../app.routes';
import { CommonModule } from "@angular/common";
import {RegistrationTeacherComponent} from "./registration-teacher/registration-teacher.component";
import {RegistrationUserComponent} from "./registration-user/registration-user.component";
import {FormComponent} from "./form/form.component";
import {RegistrationComponent} from "./registration.component";

@NgModule({
  declarations: [
    RegistrationComponent,
    RegistrationUserComponent,
    RegistrationTeacherComponent,
    FormComponent
  ],
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    HttpModule,
    routing,
    routingChild
  ]
})
export class RegistrationModule { }
