var mongoose = require('mongoose');
var avatarSchema = new mongoose.Schema({
    creator: String,
    container: String,
    original_name: String,
    name: String,
    size: Number,
    path: String
});
module.exports = mongoose.model('File', avatarSchema);