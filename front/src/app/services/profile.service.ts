import { Injectable } from '@angular/core';
import {Http} from "@angular/http";
import {Observable} from "rxjs";
import {IUser} from "../interfaces/iuser";
import {ROUTES} from "./confgis";

@Injectable()
export class ProfileService {

  constructor(private _http: Http) {}

  getUser(_idUser: string): Observable<IUser>{
    let idUser = {id : _idUser};
    return this._http.get(ROUTES.teacher +'/id'+ idUser.id).map(res => res.json());
  }

}
