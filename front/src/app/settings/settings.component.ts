import {Component, OnInit, ViewChild, ViewContainerRef, ComponentFactoryResolver} from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {SettingsTeacherComponent} from "./settings-teacher/settings-teacher.component";
import {SettingsUserComponent} from "./settings-user/settings-user.component";
import {AppStorageServices} from "../services/storage.services";

@Component({
  moduleId: module.id,
  selector: 'app-settings',
  templateUrl: 'settings.component.html',
  styleUrls: ['settings.component.css']
})
export class SettingsComponent implements OnInit {
  @ViewChild('settings', {read: ViewContainerRef}) settings;


  constructor(private activatedRoute: ActivatedRoute,
              private componentFactoryResolver: ComponentFactoryResolver,
              private viewContainerRef: ViewContainerRef) {}

  ngOnInit() {
    console.log(this.activatedRoute.snapshot.data['settings']);
    const settingsComponentResolved = this.activatedRoute.snapshot.data['settings'] ? SettingsTeacherComponent : SettingsUserComponent;
    const factory = this.componentFactoryResolver.resolveComponentFactory(<any>settingsComponentResolved);
    const ref = this.viewContainerRef.createComponent(factory);

    ref.changeDetectorRef.detectChanges();
  }

}
