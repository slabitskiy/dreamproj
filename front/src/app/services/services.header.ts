import {Headers, RequestOptions, RequestOptionsArgs, BaseRequestOptions} from "@angular/http";
import {Injectable } from "@angular/core";
import {AppStorageServices} from "./storage.services";

@Injectable()
export class BaseHeaderOptions extends BaseRequestOptions{
  constructor(private _storage: AppStorageServices){
    super()
  }
  headers = new Headers({
    'x-access-token': `${this._storage.getToken}`
  });
  merge(options?: RequestOptionsArgs): RequestOptions {
    let newOptions = super.merge(options);
    newOptions.headers.set('x-access-token', `${this._storage.getToken}`);
    return newOptions;
  }

}
