import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { LocalStorageService } from 'angular-2-local-storage';

import { AuthService } from '../services/auth.service';
import {FormValidators} from "../registration/formValidators";
import {Router} from "@angular/router";
import {APP_CONFIGS} from "../app.configs";
import {AppStorageServices} from "../services/storage.services";


@Component({
  moduleId: module.id,
  selector: 'app-login',
  templateUrl: 'login.component.html',
  styleUrls: ['login.component.css'],
  providers: [ AuthService]
})
export class LoginComponent implements OnInit {
  form: FormGroup;
  errorCode: number;

  formValidators: FormValidators = new FormValidators();

  constructor(public fb: FormBuilder,
              private _auth: AuthService,
              private _router: Router,
              private _localStorageService: LocalStorageService,
              private _storage: AppStorageServices) {

    this.form = this.fb.group({
      email: ['', [ this.formValidators.emailMatch, Validators.required]],
      password: ['', Validators.required]
    });
  }

  ngOnInit() {}

  submitForm(){
    this._auth.login(this.form.value).subscribe(
      (data: any) =>{
        this._localStorageService.set(APP_CONFIGS.localStorage.token,data.token);
        this._localStorageService.set(APP_CONFIGS.localStorage.user, data.user);
        this._storage.setToken = data.token;
        this._storage.setUser = data;
        //send emmit for listener
        this._storage.loggedUpdate();
      },
      (error) => {
       this.errorCode = error.status;
      },
      () => {
        this._router.navigate(['/dashboard']);
      }
    );
  }

/*  logout(){
    this._auth.logout().subscribe(
      (data) => {
        this._localStorageService.remove(APP_CONFIGS.localStorage.token);
        this._localStorageService.remove(APP_CONFIGS.localStorage.user);
        this._storage.setToken = {token: ''};
        this._storage.setUser = {user: ''};
      },
      (error) => {
        // console.log(error);
      }
    );
  }*/

  sess(){
    this._auth.session().subscribe(
      (data) => {
        console.log(data)
      },
      (error) => {
        console.log(error)
      }
    );
  }

  cookie(){
    console.log(this._storage.getToken);
    console.log(this._storage.getUser);
  }
}
